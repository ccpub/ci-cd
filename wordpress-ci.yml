stages:
  - build
  - deploy_prd
  - deploy_staging

image: php:7.4.13-cli-buster

build:
  stage: build
  only:
    refs:
      - master
      - main
      - develop
  artifacts:
    paths:
      - ${CI_BUILDS_DIR}/${GROUP_NAME}/${CI_PROJECT_NAME}/release.tar.gz
    expire_in: 2 hours
  script:
    - cd ${CI_PROJECT_DIR}
    - apt-get update -y -q
    - apt-get install -y -q libzip-dev zip unzip libgmp-dev libfreetype6-dev libjpeg62-turbo-dev libpng-dev git nodejs npm wget
    - docker-php-ext-install -j$(nproc) zip
    - docker-php-ext-configure gd --with-freetype --with-jpeg 
    - docker-php-ext-install -j$(nproc) gd
    - docker-php-ext-install -j$(nproc) gmp
    - curl -sS https://getcomposer.org/installer -o composer-setup.php
    - php composer-setup.php --quiet --install-dir=/usr/local/bin --filename=composer
    - rm composer-setup.php
    - cd wp-content/themes/project/
    - npm install --silent --progress=false
    - npm run --silent build
    - rm -rf ./node_modules
    - rm -rf ./sass
    - cd ${CI_PROJECT_DIR}
    # Download plugins for minisites
    - bash <(wget -qO- https://gitlab.com/ccminisites/plugin/-/raw/master/download.sh)  
    #- cd ${CI_PROJECT_DIR}
    #- cd wp-content/plugins/
    #- wget -q https://github.com/generoi/polylang-theme-translation/archive/refs/heads/master.zip
    #- unzip master.zip
    #- mv polylang-theme-translation-master polylang-theme-translation
    - cd ${CI_PROJECT_DIR}
    #- for i in `find . -name "composer.json"`; do cd $(dirname "${i}"); composer install; cd -;done;
    #- for i in `find . -name "package.json"`; do cd $(dirname "${i}"); npm install --silent --progress=false; npm run --silent build; rm -rf ./node_modules; rm -rf ./sass; cd -;done;
    - |
      tar -zcv \
      -f ../release.tar.gz ./wp-content/plugins ./wp-content/themes ./.htaccess
    - mv ../release.tar.gz .

deploy_prd:
  stage: deploy_prd
  environment:
    name: Production
  needs:
    - job: build
      artifacts: true
  only:
    refs:
      - master
      - main
  script:
    # Setup SSH deploy keys
    - apt-get update -y -q
    - apt-get install -y -q openssh-client
    - echo ${SSH_PRIVATE_KEY}
    - echo ${SSH_HOST}
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$SSH_PRIVATE_KEY")
    - mkdir -p ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - scp -P ${SSH_PORT} release.tar.gz "${SSH_USER}@${SSH_HOST}:${DOCROOT}"
    - ssh ${SSH_USER}@${SSH_HOST} -p ${SSH_PORT} "cd ${DOCROOT}; tar -xvf release.tar.gz; rm -f release.tar.gz;"
    - |
      ssh ${SSH_USER}@${SSH_HOST} -p ${SSH_PORT} sh -eof << EOT
        cd ${DOCROOT}
        wp core update
        wp core update-db
        wp plugin install polylang --activate --force --allow-root
        wp plugin install lazy-blocks --activate --force --allow-root
        wp plugin install advanced-custom-fields --activate --force --allow-root
        wp plugin install custom-post-type-ui --activate --force --allow-root
        wp plugin install disable-comments --activate --force --allow-root
        echo 1 | wp option patch insert disable_comments_options remove_everywhere
        echo 1 | wp option patch insert disable_comments_options remove_xmlrpc_comments
        echo 1 | wp option patch insert disable_comments_options remove_rest_API_comments
        wp plugin install redirection --activate --force --allow-root
        wp plugin install taxonomy-terms-order --activate --force --allow-root
        wp plugin install theme-translation-for-polylang --activate --force --allow-root
        #wp plugin activate polylang-theme-translation --allow-root
        wp plugin install tinymce-advanced --activate --force --allow-root
        wp plugin install wp-migrate-db --activate --force --allow-root
        wp plugin install autodescription --activate --force --allow-root
        wp plugin install reset-meta-box-positions --activate --force --allow-root
        wp plugin install gdpr-cookie-compliance --activate --force --allow-root
        wp plugin install wps-hide-login --activate
        wp option set whl_page ${ADMIN_PATH}
        wp option set whl_redirect_admin 404
        wp plugin install smtp-mailer --activate --force
        wp option set smtp_mailer_options '{"smtp_host":"${SMTP_HOST}","smtp_auth":${SMTP_AUTH},"smtp_username":"${SMTP_USERNAME}","smtp_password":"${SMTP_PASSWORD}","type_of_encryption":"${TYPE_OF_ENCRYPTION}","smtp_port":"${SMTP_PORT}","from_email":"${FROM_EMAIL}","from_name":"${FROM_NAME}","disable_ssl_verification":${DISABLE_SSL_VERIFICATION}}' --format=json
        wp language theme update --all
        wp language plugin update --all
        wp language core update
        wp rewrite flush
        wp transient delete --all
        wp config set DISALLOW_FILE_MODS true --raw
      EOT
    - ssh ${SSH_USER}@${SSH_HOST} -p ${SSH_PORT} "cd ${DOCROOT}; sh ./wp-content/plugins/plugins.sh;"

deploy_staging:
  stage: deploy_staging
  environment:
    name: Staging
  needs:
    - job: build
      artifacts: true
  only:
    refs:
      - develop
  script:
    # Setup SSH deploy keys
    - apt-get update -y -q
    - apt-get install -y -q openssh-client
    - echo ${SSH_PRIVATE_KEY}
    - echo ${SSH_HOST}
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$SSH_PRIVATE_KEY")
    - mkdir -p ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - scp -P ${SSH_PORT} release.tar.gz "${SSH_USER}@${SSH_HOST}:${DOCROOT}"
    - ssh ${SSH_USER}@${SSH_HOST} -p ${SSH_PORT} "cd ${DOCROOT}; tar -xvf release.tar.gz; rm -f release.tar.gz;"
    - |
      ssh ${SSH_USER}@${SSH_HOST} -p ${SSH_PORT} sh -eof << EOT
        cd ${DOCROOT}
        wp core update
        wp core update-db
        wp plugin install polylang --activate --force --allow-root
        wp plugin install lazy-blocks --activate --force --allow-root
        wp plugin install advanced-custom-fields --activate --force --allow-root
        wp plugin install custom-post-type-ui --activate --force --allow-root
        wp plugin install disable-comments --activate --force --allow-root
        echo 1 | wp option patch insert disable_comments_options remove_everywhere
        echo 1 | wp option patch insert disable_comments_options remove_xmlrpc_comments
        echo 1 | wp option patch insert disable_comments_options remove_rest_API_comments
        wp plugin install redirection --activate --force --allow-root
        wp plugin install taxonomy-terms-order --activate --force --allow-root
        wp plugin install theme-translation-for-polylang --activate --force --allow-root
        #wp plugin activate polylang-theme-translation --allow-root
        wp plugin install tinymce-advanced --activate --force --allow-root
        wp plugin install wp-migrate-db --activate --force --allow-root
        wp plugin install autodescription --activate --force --allow-root
        wp plugin install reset-meta-box-positions --activate --force --allow-root
        wp plugin install gdpr-cookie-compliance --activate --force --allow-root
        wp plugin install wps-hide-login --activate
        wp option set whl_page ${ADMIN_PATH}
        wp option set whl_redirect_admin 404
        if [ ! -d ./wp-content/plugins/hide-my-site ]; then wp plugin install hide-my-site; fi;
        wp plugin activate hide-my-site
        wp option set hide_my_site_enabled 1
        wp option set hide_my_site_password ${ACC_PASSWORD}
        wp plugin install smtp-mailer --activate --force
        wp option set smtp_mailer_options '{"smtp_host":"${SMTP_HOST}","smtp_auth":${SMTP_AUTH},"smtp_username":"${SMTP_USERNAME}","smtp_password":"${SMTP_PASSWORD}","type_of_encryption":"${TYPE_OF_ENCRYPTION}","smtp_port":"${SMTP_PORT}","from_email":"${FROM_EMAIL}","from_name":"${FROM_NAME}","disable_ssl_verification":${DISABLE_SSL_VERIFICATION}}' --format=json
        wp plugin install disable-emails --activate --force
        wp language theme update --all
        wp language plugin update --all
        wp language core update
        wp rewrite flush
        wp transient delete --all
        cp wp-content/themes/project/hmsclassic.php wp-content/plugins/hide-my-site/templates
        wp config set DISALLOW_FILE_MODS true --raw
      EOT
    - ssh ${SSH_USER}@${SSH_HOST} -p ${SSH_PORT} "cd ${DOCROOT}; sh ./wp-content/plugins/plugins.sh; sh ./wp-content/plugins/plugins-staging.sh;"
